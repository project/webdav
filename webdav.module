<?php
/**
 * @file
 *
 * Contains hooks and helper functions for module.
 */

/**
 * Implements hook_menu().
 */
function webdav_menu() {
  $items = array ();

  // Webdav Query callback
  $items['webdav'] = array (
    'page callback' => 'webdav_handler',
    'page arguments' => array (1),
    'type' => MENU_CALLBACK,
    // We should do this in order not to let drupal handle authentication. See check_auth.
    'access arguments' => array ('access content'),
  );

  // Webdav server settings
  $items['admin/settings/webdav'] = array (
    'title' => 'Server',
    'description' => 'Configure WebDAV server settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array ('webdav_admin_settings'),
    'access arguments' => array ('administer site configuration'),
    'file' => 'webdav.admin.inc',
  );

  // Main settings tab (for other modules to hook on)
  $items['admin/settings/webdav/main'] = array(
    'title' => 'Server settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function webdav_permission() {
  return array(
    'webdav access' => array(
      'title' => 'Access Webdav',
      'description' => 'Access Webdav Drupal Server',
    ),
  );
}

/**
 * Implementation of hook_cron().
 */
function webdav_cron() {
  if (webdav_is_debug()) webdav_debug("CRON: cleaning webdav expired locks");
  db_delete('webdav_locks', 'wl')
    ->condition('expires', time(), "<")
    ->execute();
}

/**
 * Check if debug logging is enabled.
 *
 * @return true if enabled.
 */
function webdav_is_debug($level=1) {
  return (webdav_debug_level()>=$level);
}

/**
 * Check if watchdog logging is enabled.
 *
 * @return true if enabled.
 */
function webdav_debug_level() {
  return variable_get("webdav_debug_level", 1);
}

/**
 * output a debug message (syslog).
 *
 * @param $message message
 */
function webdav_debug($message) {
  $lines = explode("\n", $message);
  foreach ($lines as $line) {
    error_log($line);
  }
}

/**
 * Implementation of hook_theme()
 */
function webdav_theme() {
  return array(
    'webdav_listing' => array(
      'arguments' => array('root' => NULL, 'items' => NULL),
      'template' => 'webdav_listing',
    ),
  );
}
